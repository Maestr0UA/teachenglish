package com.teachenglish_ver1;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.teachenglish_ver1.R;
import com.teachenglish_ver1.lessonWork.Lesson_activity;
import com.teachenglish_ver1.lessonWork.TestController;


public class ResultDialog extends DialogFragment implements OnClickListener{
	TextView rightAnswerView;
	TextView wrongAnswerView;
	TextView resultView;
	TestController controller;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		String str;
		setStyle(DialogFragment.STYLE_NO_TITLE, 0);
		View v = inflater.inflate(R.layout.result_lesson_layout, null);
		v.findViewById(R.id.resultDialogBtnRepeat).setOnClickListener(this);
		v.findViewById(R.id.resultDialogBtnNext).setOnClickListener(this);
		v.findViewById(R.id.resultDialogBtnExit).setOnClickListener(this);
		Bundle data = getArguments();
		rightAnswerView = (TextView) v.findViewById(R.id.resultViewRight);
		str = String.valueOf(data.getInt("Right"));
		rightAnswerView.setText(str);
		wrongAnswerView = (TextView) v.findViewById(R.id.resultViewWrong);
		str = String.valueOf(data.getInt("Wrong"));
		wrongAnswerView.setText(str);
		resultView = (TextView) v.findViewById(R.id.resultViewTotal);
		str = String.valueOf(data.getInt("Total"));
		resultView.setText(str + "%");
		return v;
	}

	
	
	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.resultDialogBtnRepeat: {
			if (getActivity() instanceof Lesson_activity) 
				((Lesson_activity) getActivity()).onRepeatLesson();
		}

		case R.id.resultDialogBtnNext: {
			if (getActivity() instanceof Lesson_activity) 
				((Lesson_activity) getActivity()).onNextLesson();
			
		}

		case R.id.resultDialogBtnExit: {
			if (getActivity() instanceof Lesson_activity) 
				((Lesson_activity) getActivity()).myFinish();
			
		}

		
		}
		
	}


	
}
