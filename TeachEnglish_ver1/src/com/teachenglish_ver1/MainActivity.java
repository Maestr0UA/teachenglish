package com.teachenglish_ver1;

import com.example.teachenglish_ver1.R;
import com.teachenglish_ver1.model.Model;
import com.teachenglish_ver1.settings.SettingsModel;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;


public class MainActivity extends Activity {
	private Model myModel;
	private MainMenuController mainMenuController;
	
	Button btnStart,btnProgress, btnSettings;
	public static MainActivity ma;
	
	
    @Override
	protected void onResume() {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    	if ((!settings.contains(SettingsModel.SET_CURRENT_LESSON)) || ( settings.getInt(SettingsModel.SET_CURRENT_LESSON, -1) == -1)) {
    			btnStart.setEnabled(false);
    	} else
    		btnStart.setEnabled(true);
		super.onResume();
	}

	@Override
	protected void onDestroy() {
    	super.onDestroy();
	}

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        ma = this;
    	super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu);
        
        myModel = Model.getInstance();
        mainMenuController = new MainMenuController(this, myModel);
        
        btnStart = (Button) findViewById(R.id.btnStart);
        btnProgress = (Button) findViewById(R.id.btnProgress);
        btnSettings = (Button) findViewById(R.id.btnSettings);
        
        btnStart.setOnClickListener(mainMenuController);
        btnProgress.setOnClickListener(mainMenuController);
        btnSettings.setOnClickListener(mainMenuController);
     

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


	


	
	

}
