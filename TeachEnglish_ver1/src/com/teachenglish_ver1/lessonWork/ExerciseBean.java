package com.teachenglish_ver1.lessonWork;

import java.util.ArrayList;

public class ExerciseBean {
	private String answer;
	private String question;
	private ArrayList<ArrayList<String>> sentences;

	public ExerciseBean(String answer, String question,
			ArrayList<ArrayList<String>> sentences) {
		this.setAnswer(answer);
		this.setQuestion(question);
		this.setSentences(sentences);
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public ArrayList<ArrayList<String>> getSentences() {
		return sentences;
	}

	public void setSentences(ArrayList<ArrayList<String>> sentences) {
		this.sentences = sentences;
	}

}
