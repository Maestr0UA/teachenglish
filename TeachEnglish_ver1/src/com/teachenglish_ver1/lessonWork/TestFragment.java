package com.teachenglish_ver1.lessonWork;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TextView;

import com.example.teachenglish_ver1.R;
import com.teachenglish_ver1.ResultDialog;
import com.teachenglish_ver1.model.LessonBean;
import com.teachenglish_ver1.model.Model;

public class TestFragment extends Fragment implements Observer{
	TestModel model;
	TestController controller;
	TextView titleView, questionView, answerView;
	int wrapContent = LinearLayout.LayoutParams.WRAP_CONTENT;
	ArrayList<String> wordList;
	TableLayout tableLayout;
	ArrayList<Button> btn;
	Button btnDelete;
	ProgressBar progressBar;

	Dialog resultExerciseDialog;
	ResultDialog resultLessonDialog;
	
	TextView resultQuestionView, resultAnswerView;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.lesson_test, container, false);
		
		tableLayout = (TableLayout) view.findViewById(R.id.tableLayout);
		
		//Bundle data = getArguments();
		//ArrayList<LessonBean> readList = data.getParcelableArrayList("LessonList");
		
		resultExerciseDialog = new Dialog(getActivity()) {
		    public boolean dispatchTouchEvent(MotionEvent event)  
		    {
		    	resultExerciseDialog.dismiss();
		        return false;
		    }
		};
		resultExerciseDialog.setContentView(R.layout.result_dialog_layout);
		resultExerciseDialog.setCanceledOnTouchOutside(true);
		resultExerciseDialog.setCancelable(true);
		
		
		resultQuestionView = (TextView) resultExerciseDialog.findViewById(R.id.resultQuestionView);
		resultAnswerView = (TextView) resultExerciseDialog.findViewById(R.id.resultAnswerView);
		
		model = new TestModel(this, Model.getInstance().getLessonsList());
		controller = new TestController(this, model);
		model.addObserver(this);

		titleView = (TextView) view.findViewById(R.id.titleView);
		titleView.setText(controller.getTitle());
		
		questionView = (TextView) view.findViewById(R.id.questionView);
		questionView.setText(controller.getQuestion());
		
		answerView = (TextView) view.findViewById(R.id.answerView);
		answerView.setText("test");
		
		btnDelete = (Button) view.findViewById(R.id.btnDelete);
		btnDelete.setOnClickListener(controller);
		
		progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
		progressBar.setMax(model.getExercises().get(model.getCurrentExercise()).getSentences().size()-1);
		progressBar.setProgress(0);
		
		resultExerciseDialog.setOnDismissListener(controller);
		
		
		
		wordList = model.getWordForDisplay();
		btn = new ArrayList<Button>();
		
		for (int i = 0; i <8; i++) {
			switch(i) {
			case 0: {
				btn.add((Button) tableLayout.findViewById(R.id.resultDialogBtnRepeat));
			}
			case 1: {
				btn.add((Button) tableLayout.findViewById(R.id.resultDialogBtnNext));
			}
			case 2: {
				btn.add((Button) tableLayout.findViewById(R.id.resultDialogBtnExit));
			}
			case 3: {
				btn.add((Button) tableLayout.findViewById(R.id.button4));
			}
			case 4: {
				btn.add((Button) tableLayout.findViewById(R.id.button5));
			}
			case 5: {
				btn.add((Button) tableLayout.findViewById(R.id.button6));
			}
			case 6: {
				btn.add((Button) tableLayout.findViewById(R.id.button7));
			}
			case 7: {
				btn.add((Button) tableLayout.findViewById(R.id.button8));
			}
			}
			btn.get(i).setOnClickListener(controller);
			if (i < wordList.size()) {
				btn.get(i).setText(wordList.get(i));
				btn.get(i).setVisibility(View.VISIBLE);
			}
			else {
				btn.get(i).setText("");
				btn.get(i).setVisibility(View.INVISIBLE);
			}

		}

		
		return view;
	}
	


	@Override
	public void update(Observable observable, Object data) {
		if (data instanceof ArrayList<?>) {
			wordList = (ArrayList<String>) data;
			int i = 0;
			while (i < 8) {
				if (i < wordList.size()) {
					btn.get(i).setText(wordList.get(i));
					btn.get(i).setVisibility(View.VISIBLE);
				} else {
					btn.get(i).setText("");
					btn.get(i).setVisibility(View.INVISIBLE);
				}	
				i++;
			}

			String str = "";
			if (model.getAnswerTitle().length() > 1) {
				str  = model.getAnswerTitle().substring(0, 1).toUpperCase() + model.getAnswerTitle().substring(1);	
				
			} else {
				str = model.getAnswerTitle().toUpperCase(Locale.ENGLISH);				
			}
			answerView.setText(str);
			questionView.setText(controller.getQuestion());

		} 
		if (data instanceof Integer) {
			progressBar.setProgress(model.getExerciseProgress());			
		}
		
	}
	
	public void showResultExerciseDialog(boolean isRight) {
		
		if (isRight) {
			resultQuestionView.setText(model.getCurrentQuestion());
			resultQuestionView.setTextColor(Color.GREEN);
			resultAnswerView.setText(model.getCurrentAnswer());
			resultAnswerView.setTextColor(Color.GREEN);
			resultExerciseDialog.setTitle("�����!");
			resultExerciseDialog.show();
		} else {
			resultQuestionView.setText(model.getCurrentQuestion());
			resultQuestionView.setTextColor(Color.GREEN);
			resultAnswerView.setText(model.getAnswerTitle());
			resultAnswerView.setTextColor(Color.RED);
			resultExerciseDialog.setTitle("�������!");
			resultExerciseDialog.show();
		}	
		
		
	}
	
	public void showResultLessonDialog(int right, int wrong, int total) {
		resultLessonDialog = new ResultDialog();
		Bundle bundle = new Bundle();
		bundle.putInt("Right", right);
		bundle.putInt("Wrong", wrong);
		bundle.putInt("Total", total);
		resultLessonDialog.setArguments(bundle);
		resultLessonDialog.show(getFragmentManager(), "");
	}

	
	
	public void dismissDialog(boolean isRight) {
		
		if (isRight) {
			resultExerciseDialog.dismiss();
		}	
		
	}

	
	
	
}
