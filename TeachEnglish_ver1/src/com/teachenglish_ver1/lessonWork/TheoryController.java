package com.teachenglish_ver1.lessonWork;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;

import com.example.teachenglish_ver1.R;

public class TheoryController implements OnClickListener {
	Fragment fragment;
	TheoryModel model;
	
	public TheoryController(Fragment fragment, TheoryModel model) {
		this.fragment = fragment;
		this.model = model;
	}

	@Override
	public void onClick(View v) {
		TestFragment testFragment = new TestFragment();
		Bundle data = new Bundle();		
		data.putParcelableArrayList("LessonList", model.lessonList);
		testFragment.setArguments(data);
		FragmentManager fragmentManager = fragment.getActivity().getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.replace(R.id.container, testFragment);
		
		fragmentTransaction.commit();
	}
	
	 
	
}
