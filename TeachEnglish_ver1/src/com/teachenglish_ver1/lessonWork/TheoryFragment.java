package com.teachenglish_ver1.lessonWork;


import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.teachenglish_ver1.R;
import com.teachenglish_ver1.model.LessonBean;
import com.teachenglish_ver1.model.Model;



public class TheoryFragment extends Fragment{
	Button goTest;
	TextView theoryView;
	
	TheoryModel model;
	TheoryController controller;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.lesson_theory, container, false);
		theoryView = (TextView) view.findViewById(R.id.theoryView);
		goTest = (Button) view.findViewById(R.id.buttonTest);
		
		//Bundle data = getArguments();
		//ArrayList<LessonBean> readList = data.getParcelableArrayList("LessonList");
		
		model = new TheoryModel(this, Model.getInstance().getLessonsList());
		controller = new TheoryController(this, model);
		
		goTest.setOnClickListener(controller); 
		theoryView.setText(Html.fromHtml(model.getLessonText()));
		
		return view;
	}
	
	
	

}
