package com.teachenglish_ver1.lessonWork;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Locale;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.example.teachenglish_ver1.R;
import com.teachenglish_ver1.ResultDialog;
import com.teachenglish_ver1.model.Model;


public class TestController implements OnClickListener, OnDismissListener{
	private TestModel model;
	private static Fragment fragment;
	Handler handler;
	
	boolean [] columnFlags;

	public TestController (Fragment fr, TestModel model) {
		this.model = model;
		TestController.fragment = fr;
		model.setCurrentExercise(0);
		columnFlags = new boolean[model.getExercises().get(model.getCurrentExercise()).getSentences().size()];
		
		handler = new MyHandler(fragment.getActivity());

	}
	
	public String getTitle () {
		return model.getLessonTitle();
	}

	public String getQuestion () {
		return model.getCurrentQuestion();
	}
	
	public boolean isFinish() {
		boolean res = true;
		for (int i = 0; i < columnFlags.length; i++) {
			if (! columnFlags[i]) {
				if (!model.getExercises().get(model.getCurrentExercise()).getSentences().get(i).get(0).equals("skp")) 
					res = false;
			}
		}
		return res;
	}
	
	@Override
	public void onClick(View v) {

		if ((v.getId() == R.id.resultDialogBtnRepeat) || (v.getId() == R.id.resultDialogBtnNext) || (v.getId() == R.id.resultDialogBtnExit) || (v.getId() == R.id.button4) || (v.getId() == R.id.button5) ||
				(v.getId() == R.id.button6) || (v.getId() == R.id.button7) || (v.getId() == R.id.button8)) {
			String text = "";
			text = ((Button) v).getText().toString();
			model.addAnswerTitle(text);
			int in = findColumnAdd(text);
			columnFlags[in] = true;
			model.setWordList(model.getCurrentExercise(), columnFlags);			
			
			if (columnFlags[columnFlags.length-1]) {	
				model.setExerciseProgress(model.getExerciseProgress()+1);
				
				
				if (model.getSettings().getAutoSkip())
					handler.sendEmptyMessageDelayed(0, 800);
				
				if (model.getAnswerTitle().equalsIgnoreCase(model.getCurrentAnswer())) {  
					model.setRightAnswer(model.getRightAnswer()+1);
					((TestFragment)fragment).showResultExerciseDialog(true);
				} else {
					((TestFragment)fragment).showResultExerciseDialog(false);
				}	
			}
 
			
		}
		
		if (v.getId() == R.id.btnDelete) {
			String str = model.getAnswerTitle().toLowerCase(Locale.ENGLISH);
			if (!str.equals("")) {
				String [] arr = str.split(" ");
				columnFlags[findColumnDelete(arr[arr.length-1])] = false;
				model.deleteAnswerTitle();
				model.setWordList(model.getCurrentExercise(), columnFlags);
			}
		}
	}
	
	private int findColumnAdd(String text) {
		ArrayList<ArrayList<String>> buffer = model.getExercises().get(model.getCurrentExercise()).getSentences();
		for (int i = 0; i < buffer.size(); i++) { 
			if (!columnFlags[i]) {
				for (int j = 0; j < buffer.get(i).size(); j++) {
					if (text.equalsIgnoreCase(buffer.get(i).get(j))) {
						return i;
					}
				}	
			}	
		}

		return -1;
	}

	private int findColumnDelete(String text) {
		ArrayList<ArrayList<String>> buffer = model.getExercises().get(model.getCurrentExercise()).getSentences();
		for (int i = buffer.size()-1; i>=0; i--) 
			if (columnFlags[i]) {
				for (int j = 0; j < buffer.get(i).size(); j++) {
					if (text.equalsIgnoreCase(buffer.get(i).get(j))) {
						return i;
					}
			}	
		}

		return -1;
	}

	
	public void nextExercise() {
		model.setCurrentExercise(model.getCurrentExercise()+1);
		if (model.getCurrentExercise() < model.getExerciseCount()) {
			columnFlags = new boolean[model.getExercises().get(model.getCurrentExercise()).getSentences().size()];
			model.setAnswerTitle("");
			model.setWordList(model.getCurrentExercise(), columnFlags);
			
		} else {
			long total = model.getRightAnswer()*100/model.getExerciseCount();
			((TestFragment)fragment).showResultLessonDialog(model.getRightAnswer(), model.getExerciseCount()-model.getRightAnswer(), (int)total);
			Model.getInstance().setProgress(model.getCurrentLesson(), (int)total);
		}	
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		
		nextExercise();
	}
	
	static class MyHandler extends Handler {
		WeakReference<Activity> wrActivity;
		
		public MyHandler(Activity activity) {
			wrActivity = new WeakReference<Activity>(activity);
		}
		
		@Override
        public void handleMessage(Message msg) {
                super.handleMessage(msg);
                Activity activity = wrActivity.get();
                if (activity != null)
                	((TestFragment) fragment).dismissDialog(true);    
        }
		
	}
	
}
