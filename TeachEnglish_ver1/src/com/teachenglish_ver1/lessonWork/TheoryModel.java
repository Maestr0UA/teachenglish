package com.teachenglish_ver1.lessonWork;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import android.support.v4.app.Fragment;

import com.teachenglish_ver1.model.LessonBean;
import com.teachenglish_ver1.settings.SettingsModel;

public class TheoryModel {
	Fragment fragment;
	public ArrayList<LessonBean> lessonList;

	SettingsModel settings;
	
	public TheoryModel(Fragment fragment, ArrayList<LessonBean> lessonList) {
		this.lessonList = lessonList;
		this.fragment = fragment;
		settings = SettingsModel.getInstance();
			
	}
	
	public String getLessonText() {
		
		
		byte[] buffer = null;
		InputStream is;
		
		try {
		    is = fragment.getActivity().getAssets().open(lessonList.get(settings.getCurrentLesson()).getPath() + "/theory.html");
		    int size = is.available();
		    buffer = new byte[size];
		    is.read(buffer);
		    is.close();
		} catch (IOException e) {
		    e.printStackTrace();
		}

		String str_data = new String(buffer);
		
		
		return str_data;
	}

	
}
