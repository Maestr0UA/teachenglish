package com.teachenglish_ver1.lessonWork;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Random;
import android.support.v4.app.Fragment;
import com.teachenglish_ver1.model.LessonBean;
import com.teachenglish_ver1.settings.SettingsModel;

public class TestModel extends Observable {
	private Fragment fragment;
	private LessonBean lesson;
	private String lessonTitle;
	private SettingsModel settings;
	private ArrayList<ExerciseBean> exercises;
	private ArrayList<String> wordForDisplay;
	private String answerTitle;
	private int currentExercise;
	private int exerciseProgressBar;
	private int rightAnswer;
	private int exerciseCount;



	public TestModel(Fragment fragment, ArrayList<LessonBean> lessonList) {
		
		this.fragment = fragment;
		settings = SettingsModel.getInstance();
		this.lesson = lessonList.get(settings.getCurrentLesson());
		exercises = getTestText();
		wordForDisplay = setWordforDisplay(0, new boolean[exercises.get(0)
				.getSentences().size()]);
		this.lessonTitle = setLessonTitle();
		
		
		answerTitle = "";
		currentExercise = 0;
		exerciseProgressBar = 0;
		rightAnswer = 0;
		exerciseCount = exercises.size();
	}

	public int getExerciseCount() {
		return exerciseCount;
	}

	public int getRightAnswer() {
		return rightAnswer;
	}


	public void setRightAnswer(int rightAnswer) {
		this.rightAnswer = rightAnswer;
	}

	public int getExerciseProgress() {
		return exerciseProgressBar;
	}

	public void setExerciseProgress(int exerciseProgress) {
		this.exerciseProgressBar = exerciseProgress;
		setChanged();
		notifyObservers(exerciseProgress);

	}
	
	public int getCurrentExercise() {
		return currentExercise;
	}

	public void setCurrentExercise(int currentExercise) {
		this.currentExercise = currentExercise;
	}


	public ArrayList<String> getWordForDisplay() {
		return wordForDisplay;
	}

	public void setWordForDisplay(ArrayList<String> wordForDisplay) {
		this.wordForDisplay = wordForDisplay;
	}

	public ArrayList<ExerciseBean> getExercises() {
		return exercises;
	}

	public void setExercises(ArrayList<ExerciseBean> exercises) {
		this.exercises = exercises;
	}

	public String getAnswerTitle() {
		return answerTitle;
	}

	public void setAnswerTitle(String answerTitle) {
		this.answerTitle = answerTitle;
	}

	public void addAnswerTitle(String text) {
		String str = getAnswerTitle() + " " + text;
		setAnswerTitle(str.trim());
	}

	public void deleteAnswerTitle() {
		String[] arr = getAnswerTitle().split(" ");
		String ss = "";
		for (int i = 0; i < arr.length - 1; i++) {
			ss += " " + arr[i];
		}
		String str = ss;
		setAnswerTitle(str.trim());
	}

	public int getCurrentLesson() {
		return settings.getCurrentLesson();
	}

	public void setCurrentLesson(int number) {
		settings.setCurrentLesson(number);
	}

	public String getCurrentQuestion() {
		return exercises.get(getCurrentExercise()).getQuestion();
	}

	public String getCurrentAnswer() {
		return exercises.get(getCurrentExercise()).getAnswer();
	}
	
	public SettingsModel getSettings() {
		return settings;
	}
	
	public void setWordList(int currentExercise, boolean[] flags) {
		wordForDisplay = setWordforDisplay(currentExercise, flags);
		setChanged();
		notifyObservers(wordForDisplay);
	}

	private ArrayList<String> setWordforDisplay(int indexExercise,
			boolean[] flags) {
		ArrayList<String> result = new ArrayList<String>();
		ArrayList<ArrayList<String>> buffer = new ArrayList<ArrayList<String>>();
		for (int i = 0; i < exercises.get(indexExercise).getSentences().size(); i++) {
			buffer.add(new ArrayList<String>());
			for (int j = 0; j < exercises.get(indexExercise).getSentences()
					.get(i).size(); j++) {
				String str = exercises.get(indexExercise).getSentences().get(i)
						.get(j);
				buffer.get(i).add(str);
			}
		}

		Random r = new Random();
		int count = 0;

		for (int i = 0; ((i < buffer.size()) && (count < 8)); i++) {
			if (!flags[i]) {
				if (buffer.get(i).size() > 0) {
					if (!buffer.get(i).get(0).equals("skp")) {
						result.add(buffer.get(i).get(0));
						count++;
					}
					buffer.get(i).remove(0);
				}
				int index = 0;
				while ((buffer.get(i).size() > 0) && (count < 8)) {
					if (buffer.get(i).size() > 1)
						index = r.nextInt(buffer.get(i).size() - 1);
					else
						index = 0;
					result.add(buffer.get(i).get(index));
					buffer.get(i).remove(index);
					count++;
				}
			}
		}
		return result;
	}

	public String getLessonTitle() {
		return this.lessonTitle;
	}
	
	private String setLessonTitle() {
		String result = "";
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(
					fragment.getActivity()
							.getAssets()
							.open(lesson.getPath() + "/Title.txt"), "UTF-8"));
			result = reader.readLine();
		} catch (IOException e) {

			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {

					e.printStackTrace();
				}
			}
		}
		return result;
	}

	private ArrayList<String> getWordFromLine(String line) {
		ArrayList<String> result = new ArrayList<String>();
		String[] array = line.split(" ");
		for (int i = 0; i < array.length; i++) {
			result.add(array[i]);
		}

		return result;
	}

	private ArrayList<ExerciseBean> getTestText() {
		ArrayList<ExerciseBean> result = new ArrayList<ExerciseBean>();
		BufferedReader reader = null;
		ArrayList<String> allLine = new ArrayList<String>();
		String line;

		try {
			reader = new BufferedReader(new InputStreamReader(fragment
					.getActivity().getAssets().open(lesson.getPath() + "/test.txt"), "UTF-8"));
			line = reader.readLine();
			while ((line = reader.readLine()) != null) {
				if (!line.equals(""))
					allLine.add(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		ArrayList<ArrayList<String>> matrix = new ArrayList<ArrayList<String>>();
		ArrayList<String> arrayLine = new ArrayList<String>();

		for (int i = 0; i < allLine.size(); i++) {
			if (allLine.get(i).startsWith("-")) {
				String question = allLine.get(i).substring(1).trim();
				String answer = "";
				for (int j = 0; j < matrix.size(); j++) {
					if (!matrix.get(j).get(0).startsWith("skp"))
						answer += matrix.get(j).get(0) + " ";
				}
				ExerciseBean exercise = new ExerciseBean(answer.trim(),
						question, matrix);
				result.add(exercise);
				matrix = new ArrayList<ArrayList<String>>();
			} else {
				arrayLine = getWordFromLine(allLine.get(i));
				matrix.add(arrayLine);
			}
		}
		return result;
	}

}
