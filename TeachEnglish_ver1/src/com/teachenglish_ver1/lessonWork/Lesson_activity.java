package com.teachenglish_ver1.lessonWork;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;

import com.example.teachenglish_ver1.R;
import com.teachenglish_ver1.ExitDialog;
import com.teachenglish_ver1.model.LessonBean;
import com.teachenglish_ver1.model.Model;
import com.teachenglish_ver1.settings.SettingsActivity;
import com.teachenglish_ver1.settings.SettingsModel;



public class Lesson_activity extends FragmentActivity {
	
	private ArrayList<LessonBean> lessonList;
	private SettingsModel settings;
	private ExitDialog exitDialog;
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.lesson);
		settings = SettingsModel.getInstance();
		if (settings.getCurrentLesson() == -1) {
			settings.setCurrentLesson(0);
			
		}
		lessonList = Model.getInstance().getLessonsList();

		exitDialog = new ExitDialog();
		exitDialog.setCancelable(false);

		
		
		//Bundle data = new Bundle();
		//data.putInt("CurrentLesson", setModel.getCurrentLesson());
		//data.putParcelableArrayList("LessonList", lessonList); 
		
		if (savedInstanceState == null) {
			if (!settings.getSkipTheory()) {
				TheoryFragment theoryFragment = new TheoryFragment();
				
				//theoryFragment.setArguments(data);
				
				FragmentManager fragmentManager = getSupportFragmentManager();
				FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
				fragmentTransaction.add(R.id.container, theoryFragment);
				fragmentTransaction.commit();
			}	
			else {
				TestFragment testFragment = new TestFragment();
				//testFragment.setArguments(data);
				FragmentManager fragmentManager = getSupportFragmentManager();
				FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
				fragmentTransaction.add(R.id.container, testFragment);
				fragmentTransaction.commit();
			}	
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.lesson_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		switch (item.getItemId()) {
		case R.id.menu_settings: {
			Intent intent = new Intent(this, SettingsActivity.class);
			startActivity(intent);
			break;
		}
		case R.id.menu_share: {
			break;
		}
		
		case R.id.menu_exit: {
			System.exit(0);
			break;
		}
		default:
		}
		return super.onOptionsItemSelected(item);
		
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			exitDialog.show(getSupportFragmentManager(), "Exit from lesson");
		}
		return super.onKeyDown(keyCode, event);
	}

	public void myFinish () {
		settings.setCurrentLesson(settings.getCurrentLesson()+1);
		finish();
	}
	
	public void onNextLesson() {
		settings.setCurrentLesson(settings.getCurrentLesson()+1);
		if (settings.getCurrentLesson() < Model.getInstance().getLessonsList().size()) {
			if (!settings.getSkipTheory()) {
				TheoryFragment theoryFragment = new TheoryFragment();
				
				//theoryFragment.setArguments(data);
				
				FragmentManager fragmentManager = getSupportFragmentManager();
				FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
				fragmentTransaction.replace(R.id.container, theoryFragment);
				fragmentTransaction.commit();
			}	
			else {
				TestFragment testFragment = new TestFragment();
				//testFragment.setArguments(data);
				FragmentManager fragmentManager = getSupportFragmentManager();
				FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
				fragmentTransaction.replace(R.id.container, testFragment);
				fragmentTransaction.commit();
			}	

		}
	}
	
	public void onRepeatLesson() {
		if (settings.getCurrentLesson() < Model.getInstance().getLessonsList().size()) {
			if (!settings.getSkipTheory()) {
				TheoryFragment theoryFragment = new TheoryFragment();
				
				//theoryFragment.setArguments(data);
				
				FragmentManager fragmentManager = getSupportFragmentManager();
				FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
				fragmentTransaction.replace(R.id.container, theoryFragment);
				fragmentTransaction.commit();
			}	
			else {
				TestFragment testFragment = new TestFragment();
				//testFragment.setArguments(data);
				FragmentManager fragmentManager = getSupportFragmentManager();
				FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
				fragmentTransaction.replace(R.id.container, testFragment);
				fragmentTransaction.commit();
			}	
		}
	}
	
}
