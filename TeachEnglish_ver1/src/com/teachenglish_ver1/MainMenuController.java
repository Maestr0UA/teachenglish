package com.teachenglish_ver1;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;

import com.example.teachenglish_ver1.R;
import com.teachenglish_ver1.lessonWork.Lesson_activity;
import com.teachenglish_ver1.model.LessonBean;
import com.teachenglish_ver1.model.Model;
import com.teachenglish_ver1.progress.ProgressActivity;
import com.teachenglish_ver1.settings.SettingsActivity;

public class MainMenuController implements OnClickListener {
	Context context;
	Model model;
	ArrayList<LessonBean> lessons;
	
	public MainMenuController(Context context, Model model) {
		this.context = context;
		this.model = model;
		lessons = model.getLessonsList();
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.btnStart: {
			Intent lessonIntent = new Intent(context, Lesson_activity.class);
			lessonIntent.putParcelableArrayListExtra("lessonList", model.getLessonsList());
			context.startActivity(lessonIntent);
			break;
		}
		case R.id.btnProgress: {
			Intent ProgressIntent = new Intent(context, ProgressActivity.class);
			ProgressIntent.putParcelableArrayListExtra("lessonList", model.getLessonsList());
			
			context.startActivity(ProgressIntent);
			break;
		}
		case R.id.btnSettings: {
			Intent SettingsIntent = new Intent(context, SettingsActivity.class);
			context.startActivity(SettingsIntent);
			break;
		}
		default: 
		}
	}
	
}
