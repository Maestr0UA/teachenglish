package com.teachenglish_ver1.progress;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.TextView;

import com.example.teachenglish_ver1.R;
import com.teachenglish_ver1.lessonWork.Lesson_activity;
import com.teachenglish_ver1.model.LessonBean;

public class ProgressAdapter extends BaseExpandableListAdapter implements OnChildClickListener{
	private Activity context;
	ArrayList<LessonBean> lessonList;
	
	ArrayList<String> parentList;
	ArrayList<ArrayList <String>> childList;
	ArrayList<ArrayList <Integer>> progressList; 
	
	public ProgressAdapter(Activity context, ArrayList<LessonBean> lessonList) {
		this.context = context;
		this.lessonList = lessonList;
		createList(lessonList);
		
		
	}
	
	private void createList(ArrayList<LessonBean> lessons) {
		this.parentList = new ArrayList<String>();
		this.childList = new ArrayList<ArrayList<String>> ();
		this.progressList = new ArrayList<ArrayList<Integer>> ();
		
		for (LessonBean lesson : lessons) {
			if (!parentList.contains(lesson.getThemeTitle())) {
				parentList.add(lesson.getThemeTitle());
				childList.add(new ArrayList<String> ());
				progressList.add(new ArrayList<Integer> ());
			}
		}
		
		for (LessonBean lesson : lessons) {
			int pos=0;
			for (int i =0; i < parentList.size(); i++) {
				if (lesson.getThemeTitle().equalsIgnoreCase(parentList.get(i))) {
					pos = i;
					break;
				}
			}
			childList.get(pos).add(lesson.getLessonTitle());
			progressList.get(pos).add(lesson.getProgress());
		}
	}
	


	@Override
	public int getGroupCount() {
		
		return parentList.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		
		return childList.get(groupPosition).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		
		return groupPosition;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		
		return childList.get(groupPosition).get(childPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.progress_group_item, null);
			
		}
		
		TextView groupText = (TextView) convertView.findViewById(R.id.group_lesson); 
		groupText.setText(parentList.get(groupPosition));
		
		
		return convertView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		
		LayoutInflater inflater = context.getLayoutInflater();
		if(convertView == null) {
			convertView = inflater.inflate(R.layout.progress_item_view, null);
			
		}
		
		TextView item = (TextView) convertView.findViewById(R.id.progressLessonView);
		TextView itemProgress = (TextView) convertView.findViewById(R.id.progressResultView);
		item.setText(childList.get(groupPosition).get(childPosition));
		itemProgress.setText("���������: "+progressList.get(groupPosition).get(childPosition) + "%");
		
		return convertView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		
		return true;
	}

	@Override
	public boolean onChildClick(ExpandableListView parent, View v,
			int groupPosition, int childPosition, long id) {

		Intent lessonIntent = new Intent(context, Lesson_activity.class);
		lessonIntent.putParcelableArrayListExtra("lessonList", lessonList);
		context.startActivity(lessonIntent);
		
		return false;
	}

}
