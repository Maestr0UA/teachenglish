package com.teachenglish_ver1.progress;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ExpandableListView;

import com.example.teachenglish_ver1.R;
import com.teachenglish_ver1.model.LessonBean;


public class ProgressActivity extends Activity {

	ExpandableListView progressView;
	private ArrayList<LessonBean> lessonList;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.progress_layout);
		lessonList = getIntent().getParcelableArrayListExtra("lessonList");
		
		
		progressView = (ExpandableListView) findViewById(R.id.ProgressListView);
		ProgressAdapter adapter = new ProgressAdapter(this, lessonList);
		progressView.setAdapter(adapter);
		for(int i=0; i < adapter.getGroupCount(); i++)
		    progressView.expandGroup(i);
		progressView.setOnChildClickListener(adapter);
		
	}
	
	
}
