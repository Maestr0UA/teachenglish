package com.teachenglish_ver1.model;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {
	 
	private static final int DB_VERSION = 1;
	static final String dbName="Progress.db";
	static final String tableName ="ProgressTable";
	static final String colProgress ="progress";
	static final String colID="ID";
	
	public static final String DATATABLE_CREATE = "create table " +tableName+ " ("+ colID + " integer primary key, " + colProgress + " integer NOT NULL);";

	private SQLiteDatabase db;
	
	
	public DBHelper(Context context) {
		super(context, dbName, null, DB_VERSION);

	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATATABLE_CREATE);
		Log.d("My Tag", "Table created");
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
	}
	
	public void saveData(ArrayList <LessonBean> list) {
		db = getWritableDatabase();
		db.delete (tableName, null, null);
		ContentValues values = new ContentValues();
		for(LessonBean lesson : list) {
			values.put(colID, lesson.getHashCode());
			values.put(colProgress, lesson.getProgress());
			//values.put(colProgress, 55);
			db.insert(tableName, null, values);
		}
		db.close();
	} 
	
	public void updateRow(int hashCode, int progress) {
		db = getWritableDatabase();
		
		ContentValues updatedValues = new ContentValues();
		updatedValues.put(colProgress, progress);
		String where = colID + "=" + hashCode;
		db.update(tableName, updatedValues, where, null);

		db.close();
	}
	
	public void insertRow(int hashCode, int progress) {
		db = getWritableDatabase();
		
		ContentValues insertValues = new ContentValues();
		insertValues.put(colID, hashCode);
		insertValues.put(colProgress, progress);
		
		db.insert(tableName, null, insertValues);
		db.close();
	}
	
	public int readRow(int hashCode) {
		int result = 0; 
		
		db = getWritableDatabase();
		String where = colID + "=" + hashCode;
		String [] columns =  new String [] {colProgress};
		String [] selectionArgs = new String[] {};
		Cursor c = db.query(tableName, columns, where, selectionArgs, null, null, null);
		if (c != null) {
			if (c.moveToFirst()) {
				do {
			          for (String cn : c.getColumnNames()) {
			        	  result = c.getInt(c.getColumnIndex(cn));
			          }
			          Log.d("MyTag", ""+result);

			        } while (c.moveToNext());
			}
			
		}
		c.close();
		db.close();
		return result;
	}
	
}