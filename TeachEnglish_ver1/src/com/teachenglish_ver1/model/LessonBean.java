package com.teachenglish_ver1.model;

import android.os.Parcel;
import android.os.Parcelable;

public class LessonBean implements Comparable<Object>, Parcelable{


	private int lessonNumber;
	private int themeNumber;
	private String themeTitle;
	private int progress;
	private String lessonTitle;
	private String path;
	private int hashCode;
	
	public LessonBean (int themeNumber, String themeTitle, int lessonNumber, String lessonTitle, String path) {
		this.themeNumber = themeNumber;
		this.themeTitle = themeTitle;
		this.lessonNumber = lessonNumber;
		this.lessonTitle = lessonTitle;
		this.path = path;
		setHashCode();
		this.progress = 0;
	}
	
	public int getLessonNumber() {
		return lessonNumber;
	}
	public void setLessonNumber(int numberLesson) {
		this.lessonNumber = numberLesson;
	}
	public int getThemeNumber() {
		return themeNumber;
	}
	public void setThemeNumber(int theme) {
		this.themeNumber = theme;
	}
	public int getProgress() {
		return progress;
	}
	public void setProgress(int progress) {
		this.progress = progress;
	}
	
	public String getThemeTitle() {
		return themeTitle;
	}
	public void setThemeTitle(String themeTitle) {
		this.themeTitle = themeTitle;
	}

	@Override
	public int compareTo(Object another) {
		LessonBean other = (LessonBean) another;
		/*
		if (this.getLevelNumber() > other.getLevelNumber()) return 1;
		else
			if (this.getLevelNumber() < other.getLevelNumber()) return -1;
			else //if level is equal
				if (this.getThemeNumber() > other.getThemeNumber()) return 1;
				else
					if (this.getThemeNumber() < other.getThemeNumber()) return -1;
					else //if theme is equal
						if (this.getLessonNumber() > other.getLessonNumber()) return 1;
						else 
							if (this.getLessonNumber() < other.getLessonNumber()) return -1;
							else return 0; // if lesson is totally equal!
		*/
		return this.hashCode > other.hashCode? 1: -1;
		
	}

	public String getLessonTitle() {
		return lessonTitle;
	}

	public void setLessonTitle(String lessonTitle) {
		this.lessonTitle = lessonTitle;
	}

	public int getHashCode() {
		return hashCode;
	}

	public void setHashCode() {
		hashCode = themeNumber*100+lessonNumber;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	@Override
	public int describeContents() {
		
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int flags) {
		parcel.writeInt(themeNumber);
		parcel.writeString(themeTitle);
		parcel.writeInt(lessonNumber);
		parcel.writeString(lessonTitle);
		parcel.writeInt(progress);
		parcel.writeString(path);
		parcel.writeInt(hashCode);
	}

	public static final Parcelable.Creator<LessonBean> CREATOR = new Parcelable.Creator<LessonBean>() {

		@Override
		public LessonBean createFromParcel(Parcel source) {
			return new LessonBean(source);
		}

		@Override
		public LessonBean[] newArray(int size) {
			return new LessonBean[size];
		}
	};
	
	private LessonBean(Parcel parcel) {
		themeNumber = parcel.readInt();
		themeTitle = parcel.readString();
		lessonNumber = parcel.readInt();
		lessonTitle = parcel.readString();
		progress = parcel.readInt();
		path = parcel.readString();
		hashCode = parcel.readInt();
	}
}
