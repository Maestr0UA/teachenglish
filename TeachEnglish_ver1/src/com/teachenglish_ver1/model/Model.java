package com.teachenglish_ver1.model;


import java.util.ArrayList;

import com.teachenglish_ver1.MainActivity;


public class Model {
	private ArrayList <LessonBean> lessonsList;
	private DBHelper dbHelper;
	
	private Model () {
		lessonsList = new Lessons(MainActivity.ma).getLessonsTree();
		dbHelper = new DBHelper(MainActivity.ma);
		fillProgress();
	}
	
	private static class ModelHolder {
		private final static Model instance = new Model();
	}
	
	public static Model getInstance() {
		return ModelHolder.instance;
	}
	
	
	public ArrayList<LessonBean> getLessonsList() {
		return lessonsList;
	}

	private void fillProgress() {
		for (LessonBean lesson : lessonsList) {
			lesson.setProgress(dbHelper.readRow(lesson.getHashCode()));
		}
	}
	
	public void setProgress(int index, int progress) {
		dbHelper.updateRow(lessonsList.get(index).getHashCode(), progress);
		lessonsList.get(index).setProgress(progress);
		
		
	}
	
}
