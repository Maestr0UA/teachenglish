	package com.teachenglish_ver1.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import android.content.Context;
import android.content.res.AssetManager;

public class Lessons {
	Context context;
	
	ArrayList<LessonBean> lessons;
	
	
	Lessons (Context context) {
		this.context = context;
	
		lessons = new ArrayList<LessonBean>();
		readFromAssets();
	}
	
	public ArrayList<LessonBean> getLessonsTree() {
		return lessons;
	}
	
	

	class SubClass {
		int number;
		String title;
		
		SubClass(int i, String str) {
			number = i;
			title = str;
		}
	}
	
	
	public SubClass getSubStrings(String str) {
		int pos = str.indexOf(" ");
		String num = str.substring(0, pos);
		int number = Integer.valueOf(num).intValue();
		String title = str.substring(pos, str.length());
		
		return new SubClass(number, title);
	}
	
	public void readFromAssets() {
		String [] tempList, listTheme; 
		ArrayList<String> listLesson;
		AssetManager am = context.getResources().getAssets();
		
		
		try {
			listTheme = am.list("Exercises");
			
			for (int i = 0; i < listTheme.length; i++) {
				tempList = am.list("Exercises/" + listTheme[i]);
				listLesson = new ArrayList<String>();
				for (String str : tempList) {
					if (!str.equalsIgnoreCase("title.txt")) {
						listLesson.add(str);
					}
				}
				
				for (int j = 0; j < listLesson.size(); j++) {
					String path = "Exercises/" + listTheme[i] + "/" + listLesson.get(j);
					
					LessonBean les = new LessonBean(i, getTitle(am, "Exercises/" + listTheme[i]), j, getTitle(am, path), path);
					lessons.add(les);
				}
				
			}
		
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	private String getTitle(AssetManager am, String path) {
		String result = "";
		BufferedReader reader = null;
		try {
		    reader = new BufferedReader(new InputStreamReader(am.open(path + "/Title.txt"), "UTF-8")); 
		    result = reader.readLine();
		} catch (IOException e) {
			
			e.printStackTrace();
		} finally {
		    if (reader != null) {
		        try {
					reader.close();
				} catch (IOException e) {
					
					e.printStackTrace();
				}
		    }
		}
		return result;
	}
	
	
}
