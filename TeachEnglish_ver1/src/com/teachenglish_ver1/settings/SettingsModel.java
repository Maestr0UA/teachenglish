package com.teachenglish_ver1.settings;

import java.util.Observable;

import com.teachenglish_ver1.MainActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SettingsModel extends Observable {
	public static final String SET_THEME = "set_theme";
	public static final String SET_FONT_COLOR = "set_color_font";
	public static final String SET_TEXT_COLOR = "set_color_text";
	public static final String SET_AUTO_CHECK = "set_auto_check";
	public static final String SET_AUTO_SKIP = "set_auto_skip";
	public static final String SET_SKIP_THEORY = "set_skip_theory";
	public static final String SET_CURRENT_LESSON = "currentlesson";
	public static int currentLesson = 0;
	
	SharedPreferences settings;
	
	private SettingsModel () {
		 settings = PreferenceManager.getDefaultSharedPreferences(MainActivity.ma.getApplicationContext());
		 SettingsCheck(); // ���������� �� ����� ��������
		
	}
	
	private static class SettingsHolder {
		private static final SettingsModel instance = new SettingsModel();
	} 
	
	public static SettingsModel getInstance () {
		return SettingsHolder.instance;
	}
	
	
	public boolean getAutoCheck() {
		return settings.getBoolean(SET_AUTO_CHECK, false);
		
	}

	public boolean getAutoSkip() {
		return settings.getBoolean(SET_AUTO_SKIP, false);
	}
	

	public boolean getSkipTheory() {
		return settings.getBoolean(SET_SKIP_THEORY, false);
	}
	
	public int getCurrentLesson() {
		return settings.getInt(SET_CURRENT_LESSON, 0);
	}

	public void setAutoCheck(boolean set) {
		settings.edit().putBoolean(SET_AUTO_CHECK, set).commit();
	}
	
	public void setAutoSkip(boolean set) {
		settings.edit().putBoolean(SET_AUTO_SKIP, set).commit();
	}

	public void setSkipTheory(boolean set) {
		settings.edit().putBoolean(SET_SKIP_THEORY, set).commit();
	}

	public void setCurrentLesson(int set) {
		settings.edit().putInt(SET_CURRENT_LESSON, set).commit();
	}
	
	private void SettingsCheck() {
    	
    	if (!settings.contains(SET_CURRENT_LESSON)) {
    		settings.edit().putInt(SET_CURRENT_LESSON, -1).commit();
    	}
    	if (!settings.contains(SET_SKIP_THEORY.toString())) {
    		settings.edit().putBoolean(SET_SKIP_THEORY, false).commit();
    	}
    	if (!settings.contains(SET_AUTO_CHECK.toString())) {
    		settings.edit().putBoolean(SET_AUTO_CHECK, false).commit();
    	}
    	if (!settings.contains(SET_AUTO_SKIP.toString())) {
    		settings.edit().putBoolean(SET_AUTO_SKIP, false).commit();
    	}
    }
	
}
