package com.teachenglish_ver1.settings;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import com.example.teachenglish_ver1.R;

public class SettingsActivity extends PreferenceActivity{

	
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.settings_layout);

	}
}
