package com.teachenglish_ver1.settings;

public class SettingsController {
	private SettingsModel setModel;
	
	
	public SettingsController(SettingsModel model) {
		setModel = model;
	} 
	
	public void setCurrentLesson(int set) {
		setModel.setCurrentLesson(set);
	}
	
	public int getCurrentModel() {
		return setModel.getCurrentLesson();
	}
	
	
	
	
}
